/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "Import/IModelImporter.h"
#include "Import/ModelImportConfiguration.h"


struct aiScene;
struct aiNode;
struct aiMesh;

class UGenericTwinTextureCache;

namespace GenericTwin {

struct SSceneNode;
struct SSceneMesh;

class FAssimpImporter	:	public IModelImporter
{
public:

	static FAssimpImporter* Create();

	virtual GenericTwin::ScenePtr ImportModel(const FString &PathToModel, const SModelImportConfiguration &ImportConfiguration) override;

	virtual FString GetImporterName() const override;

private:

	FAssimpImporter();

	virtual ~FAssimpImporter();
	
	void ImportNode(const struct aiNode &SrcNode, GenericTwin::SSceneNode &DstNode, const FTransform &ParentTransform);

	GenericTwin::SSceneMesh* ImportMesh(const struct aiMesh &SrcMesh);

	FString ImportMaterial(uint32 MaterialIndex);

	SModelImportConfiguration				m_ImportConfiguration;

	const aiScene							*m_SrcScene = 0;

	GenericTwin::SScene						*m_Scene = 0;

	TMap<int32, GenericTwin::SSceneMesh*>	m_ImportedMeshes;

	TMap<uint32, FString>					m_MaterialMap;

	UGenericTwinTextureCache				*m_TextureCache = 0;

};

}	//	namespace GenericTwin
